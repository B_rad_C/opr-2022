#!/usr/bin/env python3
import logging
from logging.handlers import RotatingFileHandler
import requests
from os.path import join, dirname


log = logging.getLogger('opr-monitor')
log.setLevel(logging.INFO)
fh = RotatingFileHandler(join(dirname(__file__), 'opr-monitor.log'), maxBytes=5242880, backupCount=5)
fh.setFormatter(logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'))
log.addHandler(fh)

DONATE_URL = 'https://opr2022.org/en/donate/'

try:
    response = requests.get(DONATE_URL)
    response.raise_for_status()
    log.info(f'DONATE_URL_ONLINE status_code={response.status_code}')
except Exception as e:
    log.error('ERROR_CHECKING_DONATE_URL', exc_info=True)
