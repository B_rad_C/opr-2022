# OPR 2022 Monitor

This script is intended to log errors if the donate page is down.

It is written for python 3.6 installed on the digital ocean droplet

## Setup
##### Install code

    cd <user folder>
    git clone https://B_rad_C@bitbucket.org/B_rad_C/opr-2022.git
    cd opr-2022
    python3 -m venv venv
    source venv/bin/activate
    python3 -m pip install --upgrade pip
    python3 -m pip install -r requirements.txt
    
##### Create cron job
open cron tab editor

    crontab -e
    
insert the following line

    */5 * * * * <path to venv python> <path to monitor.py>
    
    
## Read logs

    cd <opr-2022 installation folder>
    source venv/bin/activate
    python -m kwogger opr-monitor.log